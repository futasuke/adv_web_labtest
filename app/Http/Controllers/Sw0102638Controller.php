<?php

namespace App\Http\Controllers;

use App\sw0102638;
use Illuminate\Http\Request;

class Sw0102638Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $student = sw0102638::all();

        return view('index',compact('student'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validatedData = $request->validate([
            'year' => 'numeric|required|max:2025|min:2018',
            'sem' => 'numeric|required',
            'cgpa' => 'numeric|required|between:0.00,4.00',
        ]);

        $student = new sw0102638;
        $student->name = $request->input('name');
        $student->year = $request->input('year');
        $student->semester = $request->input('sem');
        $student->cgpa = $request->input('cgpa');
        $student->save();

        return redirect()->route('sw0102638.index')->with('success_message', 'Data successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sw0102638  $sw0102638
     * @return \Illuminate\Http\Response
     */
    public function show(sw0102638 $sw0102638)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sw0102638  $sw0102638
     * @return \Illuminate\Http\Response
     */
    public function edit(sw0102638 $sw0102638)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sw0102638  $sw0102638
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sw0102638 $sw0102638)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sw0102638  $sw0102638
     * @return \Illuminate\Http\Response
     */
    public function destroy(sw0102638 $sw0102638)
    {
        //
    }
}
