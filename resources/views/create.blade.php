<!DOCTYPE html>
<html>
<head>
	<title>Add new data</title>
</head>
<body>
	@if ($errors->any())
      <div class="alert alert-danger" style="color: red">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
	@endif

	<form method="post" action="{{route('sw0102638.store')}}">
		@csrf
		Name : <input type="text" name="name"><br>
		Year : <input type="integer" name="year"><br>
		Semester : <input type="integer" name="sem"><br>
		CGPA : <input type="double" name="cgpa"><br>
		<br>
		<input type="submit" value="submit">
	</form>
</body>
</html>