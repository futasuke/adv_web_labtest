<!DOCTYPE html>
<html>
<head>
	<title>Homepage</title>
</head>
<body>
	@if(session()->has('success_message'))
        	<p style="color:green;">{{ session()->get('success_message') }}</p>
	@endif
	<table border="1" cellpadding="10">
	  	<tr>
		    <th>Name</th>
		    <th>Year</th> 
		    <th>Semester</th>
		    <th>CGPA</th>
	  	</tr>
	  	@foreach($student as $s)
		<tr>
			    <td>{{$s->name}}</td>
			    <td>{{$s->year}}</td> 
			    <td>{{$s->semester}}</td>
			    <td>{{$s->CGPA}}</td>
		</tr>
		@endforeach	
	</table>
	<br><br>
	<button onclick="goTo()">Add new record</button>

	<script type="text/javascript">
		function goTo() {
			window.location = "{{route('sw0102638.create')}}";
		}
	</script>
</body>
</html>